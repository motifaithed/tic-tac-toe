let editedPlayer = 0;
let selectedPlayer = 0;
let round = 1;

let gameData = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
];

const players = [
    {
        name:'',
        symbol:'X'
    },
    {
        name:'',
        symbol:'O'
    }
];

const playerConfigOverlayElement = document.getElementById('configuration-overlay');
const backDropElement = document.getElementById('backdrop');
const formElement = document.querySelector('form');
const errorsElement = document.getElementById('config-errors');
const gameAreaElement = document.getElementById('active-game');
const gameElementLists = document.querySelectorAll('#game-board li');
const activePlayerElement = document.getElementById('active-player-name');
const gameOverElement = document.getElementById('game-over');
const winnerNameElement = document.getElementById('winner-name');

const editPlayer1ButtonElement = document.getElementById('btn-edit-player-1');
const editPlayer2ButtonElement = document.getElementById('btn-edit-player-2');
const playerConfigCancelBtnElement = document.getElementById('cancel-overlay');
const startNewGameBtnElement = document.getElementById('btn-start-new-game');

editPlayer1ButtonElement.addEventListener('click', openPlayerConfig);
editPlayer2ButtonElement.addEventListener('click', openPlayerConfig);
startNewGameBtnElement.addEventListener('click', startNewGame);

backDropElement.addEventListener('click', closePlayerConfig);
playerConfigCancelBtnElement.addEventListener('click', closePlayerConfig);

formElement.addEventListener('submit', addPlayerConfig);

for(const gameElementList of gameElementLists){
    gameElementList.addEventListener('click', selectElement);
}

