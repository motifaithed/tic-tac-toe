function startNewGame() {
  if (players[0].name === "" || players[1].name === "") {
    alert("please configure players first");
    return;
  }
  editedPlayer = 0;
  selectedPlayer = 0;
  round = 1;
  gameData = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
  ];

  for(const gameElementList of gameElementLists){
    gameElementList.textContent = '';
    gameElementList.classList.remove('disabled');
}
  activePlayerElement.textContent = players[selectedPlayer].name;
  gameAreaElement.style.display = "block";
  document.getElementById('player-turn').style.display = 'block';

gameOverElement.style.display = 'none';
}

function switchPlayer() {
  if (selectedPlayer === 0) {
    selectedPlayer = 1;
  } else {
    selectedPlayer = 0;
  }
}
function selectElement(event) {
  const eventElement = event.target;
  let selectedElementRow = eventElement.dataset.row - 1;
  let selectedElementCol = eventElement.dataset.col - 1;

  if (gameData[selectedElementRow][selectedElementCol]) {
    alert("please select an empty field");
    return;
  }

  gameData[selectedElementRow][selectedElementCol] = selectedPlayer + 1;
  eventElement.textContent = players[selectedPlayer].symbol;
  eventElement.classList.add("disabled");

  switchPlayer();
  
  
  winnerIndex = checkGameOver();
  round++;

 if(winnerIndex > 0){
    const winner = winnerIndex - 1;
    gameOverElement.style.display = 'block';
    winnerNameElement.textContent = players[winner].name;
    document.querySelector('#player-turn').style.display = 'none';
 }
 if(winnerIndex === -1){
    gameOverElement.style.display = 'block';
    document.querySelector('#game-over h2').textContent = 'It\`s a draw!';
    document.querySelector('#player-turn').style.display = 'none';
 }
  activePlayerElement.textContent = players[selectedPlayer].name;

}

function checkGameOver(){
    //checking for row winner
    for (let i = 0; i < 3; i++){
        if(gameData[i][0] && gameData[i][0] === gameData[i][1] && gameData[i][0] === gameData[i][2]){
            return gameData[i][0];
        }
    }
    //checking for column winner
    for(let i = 0; i < 3; i++){
        if(gameData[0][i] && gameData[0][i] === gameData[1][i] && gameData[0][i] === gameData[2][i]){
            return gameData[0][i];
        }
    }
    //checking for diagonal top left bottom right
    if(gameData[0][0] && gameData[0][0] === gameData[1][1] && gameData[0][0] === gameData[2][2]){
        return gameData[0][0];
    }
    //checking for diagonal bottom left top right
    if(gameData[2][0] && gameData[2][0] === gameData[1][1] && gameData[2][0] === gameData[0][2]){
        return gameData[2][0];
    }
    //check for draw
    if(round === 9){
        return -1;
    }
    return 0;
}
