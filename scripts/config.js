function openPlayerConfig(event) {
  editedPlayer = event.target.dataset.playerid;

  playerConfigOverlayElement.style.display = "block";
  backDropElement.style.display = "block";
}

function closePlayerConfig() {
  playerConfigOverlayElement.style.display = "none";
  backDropElement.style.display = "none";
  document
    .querySelector("#configuration-overlay input")
    .classList.remove("error");
  document
    .querySelector("#configuration-overlay label")
    .classList.remove("error");
  errorsElement.textContent = "";
  document.getElementById("player-name").value = "";
}

function addPlayerConfig(event) {
  event.preventDefault();

  const formData = new FormData(event.target);
  const playerName = formData.get("playerName").trim();

  if (!playerName) {
    errorsElement.textContent = "Please input a valid name!";
    document
      .querySelector("#configuration-overlay input")
      .classList.add("error");
    document
      .querySelector("#configuration-overlay label")
      .classList.add("error");
    return;
  }

  const selectedPlayerElement = document.getElementById(
    "player-name-" + editedPlayer
  );
  selectedPlayerElement.textContent = playerName;

  players[editedPlayer - 1].name = playerName;

  closePlayerConfig();
}
